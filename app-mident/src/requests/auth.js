import axios from 'axios';
import config from '../config/secrets';

function login(credentials){
    return axios.post(`${config.url}/doctores/auth`, credentials)
}

export { login }