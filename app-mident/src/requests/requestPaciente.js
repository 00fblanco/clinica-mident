import axios from 'axios';
import config from '../config/secrets';

function create(data, jwt){
    axios.defaults.headers.common['Authorization'] = `Bearer ${jwt}`;
    return axios.post(`${config.url}/api/pacientes`, data)
}

function getPacientes(jwt){
    axios.defaults.headers.common['Authorization'] = `Bearer ${jwt}`;
    return axios.get(`${config.url}/api/pacientes/`);
}

export { create, getPacientes }