import axios from 'axios';
import config from '../config/secrets';

function getPaciente(id, jwt){
    axios.defaults.headers.common['Authorization'] = `Bearer ${jwt}`;
    return axios.get(`${config.url}/api/pacientes/${id}`);
}

// fetch to get antecentes
function getAntecedentes(id, jwt){
    pushJWT(jwt);
    return axios.get(`${config.url}/api/pacientes/${id}/antecedentes`);
}


// agregar contacto de emergencia
function addContacto(data, jwt){
    axios.defaults.headers.common['Authorization'] = `Bearer ${jwt}`;
    return axios.post(`${config.url}/api/pacientes/contactos_emergencia`, data)
}

// agregar antecedentes heredo familair
function addHeredoFamiliar(data, jwt){
    pushJWT(jwt);
    return axios.post(`${config.url}/api/pacientes/antecedentes/`, data);
}

function pushJWT(jwt){
    axios.defaults.headers.common['Authorization'] = `Bearer ${jwt}`;
}

function update(data, jwt){
    axios.defaults.headers.common['Authorization'] = `Bearer ${jwt}`;
    return axios.put(`${config.url}/api/pacientes/${data.id}`, data)
}

export {  getPaciente, getAntecedentes, update, addContacto, addHeredoFamiliar }