import React, { Component } from 'react';
import { Button, Row, Col, Form, Icon, Input, Card, message} from 'antd';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { login } from '../requests/auth';
import * as doctorActions from '../actions/doctorActions';

const FormItem = Form.Item;


class Login extends Component {
    constructor(props){
        super(props);
        this.auth = this.auth.bind(this);
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const credentials = {
                    correo: values.correo,
                    contrasenia: values.contrasenia
                }
                login(credentials)
                    .then(this.auth)
                    .catch((err) => {
                        if(err.response){
                            message.error(err.response.data.msg);
                        }
                    })
            }
        });
    }
    auth(data){
        data = data.data;
        this.props.login(data.jwt);
        // this.props.dispatch(doctorActions.login(data.jwt));
        this.props.loadDoctor(data.doctor);
        // this.props.dispatch(doctorActions.loadDoctor(data.doctor));
        this.props.history.push('/');
    }
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div className="full-window" style={{backgroundImage: 'url("/images/background.svg")', backgroundSize: 'cover', backgroundRepeat: 'no-repeat'}}>
                <Row type="flex" justify="center" align="middle" className="full-window">
                    <Col lg={12} xs={22} >
                        <Card className="card" >
                            <Row type="flex" justify="center" align="middle" style={{paddingTop: '3em', paddingBottom: '3em'}}>
                                <Col lg={20} xs={24} style={{textAlign: 'center'}}> 
                                    <h1 style={{color: '#283d52'}}>Clinica dental <strong style={{color: '#2592fc'}}>MiDent</strong></h1>
                                    <h2 style={{color: '#283d52'}}>Autenticación</h2>
                                </Col>
                                <Col lg={20} xs={24}> 
                                    <Form onSubmit={this.handleSubmit} className="login-form">
                                    <FormItem
                                        label="Correo"
                                        >
                                        {getFieldDecorator('correo', {
                                            rules: [{ required: true, message: 'El correo es obligatorio' }],
                                        })(
                                            <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Introduzca su correo" />
                                        )}
                                    </FormItem>
                                    <FormItem
                                        label="Contraseña"
                                        >
                                        {getFieldDecorator('contrasenia', {
                                            rules: [{ required: true, message: 'La contrasenia es obligatoria'}],
                                        })(
                                            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Introduzca su contraseña" />
                                        )}
                                    </FormItem>
                                    <Button style={{ width: '100%'}} type="primary" htmlType="submit" icon="login">Autenticarse</Button>
                                    </Form>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
                
            </div>
        );
    }
}

function mapStateToProps(state, ownProps){
    return {
        doctor: state.doctor
    }
}
export default withRouter(connect(mapStateToProps, { login:doctorActions.login, loadDoctor: doctorActions.loadDoctor })(Form.create()(Login)));