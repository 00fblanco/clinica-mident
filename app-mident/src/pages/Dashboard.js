import React, { Component } from 'react';
import { Layout, Menu, Icon, Button, Row, Col, Select} from 'antd';
import uuid from 'uuid';
import { withRouter, Switch} from 'react-router-dom';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';

import notFound from '../components/notFound';
import HistorialNuevo from './historiales/HistorialNuevo';
import Historial from './historiales/Historial';
import Home from './Home';

import { logout } from '../actions/doctorActions';
import * as pacienteActions from '../actions/pacienteActions';

const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;
const Option = Select.Option;



class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            collapsed: true,
            search: false,
            selectedKey: 0
        }

        this.goHome = this.goHome.bind(this);
        this.handleMenu = this.handleMenu.bind(this);
        this.logout = this.logout.bind(this);
        this.handlePacienteSelected = this.handlePacienteSelected.bind(this);
        this.loadPacientes();
    }
    loadPacientes(){
        const { doctor } = this.props;
        this.props.dispatch(pacienteActions.getPacientes(doctor.jwt));
    }
    logout(){
        this.props.dispatch(logout());
        this.props.history.push('/');
    }
    goHome(){
        this.props.history.push('/');
    }
    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed
        })
    }
    handleMenu({item, key, selectedKeys}){
        // # key(1) Agregar historial medico
        // # key(2) Buscar historial
        this.setState({
            selectedKey: key,
            search: false
        })
        var path = "";
        switch (key) {
            case '1':
                path = '/'
                break;
            case "2":
                    path = '/historiales/new'
                break;
            default:
                path = ''
        }
        this.props.history.push(path);
    }
    handlePacienteSelected(id_paciente){
        this.props.history.push(`/historiales/${id_paciente}`);
        this.setState({
            search: true
        })
    }
    render() {
        const { doctor, pacientes } = this.props;
        const { search } = this.state;
        return (
                <Layout className="full-window">
                    <Sider
                        breakpoint="lg"
                        trigger={null}
                        collapsible
                        collapsed={this.state.collapsed}
                        style={{ background: '#fff'}}
                    >
                        <div className="logo" style={{textAlign: 'center', color: '#2592fc'}}>
                             <img src="/images/mident.png" alt="dentista" height="50%"/>
                            {this.state.collapsed ? <h1 style={{color: '#2592fc'}} >MD</h1> :
                                <h1 style={{color: '#2592fc'}}>MiDent</h1>
                            }
                        </div>

                        <Menu theme="light" mode="inline" defaultSelectedKeys={['0']} selectedKeys={this.state.search ? ['0']: [`${this.state.selectedKey}`]} onSelect={this.handleMenu}>
                            <Menu.Item key="1" >
                                <Icon type="home" />
                                <span>Inicio</span>
                            </Menu.Item>
                            <SubMenu key="sub1" title={<span><Icon type="solution" /><span>Historial clínico</span></span>}>
                                <Menu.Item key="2">Nuevo</Menu.Item>
                            </SubMenu>
                        </Menu>
                    </Sider>
                    <Layout>
                        <Header style={{ background: '#fff', padding: 0}}>
                            <Icon
                            className="trigger"
                            type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                            onClick={this.toggle}
                            />
                            <span style={{marginRight: '5px'}}>
                                <Icon type="search" />
                            </span>
                            <span >
                                <Select   
                                    allowClear
                                    onChange={this.handlePacienteSelected}
                                    style={{ width: '300px'}}
                                    showSearch
                                    showArrow={false}
                                    placeholder="Buscar un paciente por nombre"
                                    optionFilterProp="children"
                                    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                    >
                                    {
                                        pacientes && pacientes.map((paciente, index) => {
                                            return (<Option key={index} value={`${paciente.id}`} >{`${paciente.nombre} ${paciente.ap_paterno} ${paciente.ap_materno}`}</Option>)
                                        })
                                    }
                                </Select>
                            </span>
                            <Row style={{float: 'right', marginRight: '2em'}}>
                                <Col xs={0} md={22}>
                                    <span style={{marginRight: '10px'}}><strong>Bienvenido Dr. {`${doctor.nombre} ${doctor.ap_paterno}`} </strong></span>
                                </Col>
                                <Col md={2}>
                                    <span>
                                        <Button type="danger" icon="logout" shape="circle" onClick={this.logout}></Button>
                                    </span>
                                </Col>
                            </Row>
                        </Header>
                        <Content key={uuid.v1()} style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280 }}>
                            <Switch>
                                <Route exact path="/historiales/new" component={HistorialNuevo}/>
                                <Route exact path="/historiales/:id" component={Historial}/>
                                <Route exact path="/" component={Home} />
                                <Route component={notFound}/>
                            </Switch>
                        </Content>
                    </Layout>
                </Layout>
        );
    }
}
function mapStateToProps(state, ownProps) {
    return {
        doctor: state.doctor,
        pacientes: state.pacientes,
    }
}
export default connect(mapStateToProps)(withRouter(Dashboard));