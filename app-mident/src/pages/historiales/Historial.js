import React, { Component } from 'react';
import { withRouter }from 'react-router-dom';
import { connect } from 'react-redux';
import { Spin, Row, Col, message, Tabs, Icon } from 'antd';
import uuid from 'uuid';

import * as pacienteActions from '../../actions/pacienteActions';
import * as historialActions from '../../actions/historialActions';
import * as requestPaciente from '../../requests/requestPaciente';
import * as requestHistorial from '../../requests/requestHistorial';

import FormSalud from '../../components/historial/FormSalud';
import FormHistorial from '../../components/historial/FormHistorial';
import FormAntecedentes from '../../components/historial/FormAntecedentes';
import FormPlanTratamiento from '../../components/historial/FormPlanTratamiento';
import FormConsentimiento from '../../components/historial/FormConsentimiento';
import FormEstadoCuenta from '../../components/historial/FormEstadoCuenta';


const TabPane = Tabs.TabPane;
class Historial extends Component {
    constructor(props){
        super(props);
        const id_paciente = this.props.match.params.id;
        this.loadPaciente(id_paciente);
        this.loadAntecedentes(id_paciente);
        
        this.state = {
            historial: {}
        }
        this.loadPaciente = this.loadPaciente.bind(this);
        this.loadAntecedentes = this.loadAntecedentes.bind(this);

        this.loader = this.loader.bind(this);

        this.update = this.update.bind(this);
        this.addContacto = this.addContacto.bind(this);
        this.addHeredoFamiliar = this.addHeredoFamiliar.bind(this);
        
    }

    loadPaciente(id_paciente){
        const { doctor } = this.props;
        this.props.dispatch(historialActions.getPaciente(id_paciente, doctor.jwt));
    }
    loadAntecedentes(id_paciente){
        const { doctor } = this.props;
        this.props.dispatch(historialActions.getAntecedentes(id_paciente, doctor.jwt));
    }
    update(data){
        requestHistorial.update(data, this.props.doctor.jwt)
            .then(res => {
                if(res.status === 200){
                    message.success('Paciente actualizado satisfactoriamente!!');
                }else{
                    message.warning('Algo salio mal :o, recargar la pagina y verificar datos.')
                }
                
            }).catch(err => {
                console.log(err);
                if(err.response){
                    message.error(err.response.data.msg);
                }
            })
    }
    addContacto(data){
        requestHistorial.addContacto(data, this.props.doctor.jwt)
            .then(res => {
                if(res.status === 200){
                    message.success('Contacto de emergencia agregado satisfactoriamente!!');
                    this.props.dispatch(historialActions.addContacto(res.data));
                }else{
                    message.warning('Algo salio mal :o, recargar la pagina y verificar datos.')
                }
            }).catch(err => {
                console.log(err);
                if(err.response){
                    message.error(err.response.data.msg);
                }
            })
    }
    addHeredoFamiliar(data){
        console.log('data', data)
        data['id_paciente'] = this.props.paciente.id;
        data.enfermedades.map(enfermedad => {
            let heredoFamiliar = {
                id_paciente: data.id_paciente,
                parentesco_familiar: data.parentesco,
                id_enfermedad: enfermedad
            }
            requestHistorial.addHeredoFamiliar(heredoFamiliar, this.props.doctor.jwt)
                .then(res => {
                    if(res.status === 200){
                        message.success('Antecedente heredo-familiar agregado satisfactoriamente!!');
                        this.props.dispatch(historialActions.addHeredoFamiliar(res.data))
                    }else{
                        message.warning('Algo salio mal :o, recargar la pagina y verificar datos.')
                    }
                }).catch(err => {
                    console.log(err);
                    if(err.response){
                        message.error(err.response.data.msg);
                    }
                })
        })
    }
    loader(){
        const { paciente, antecedentes} = this.props;
        if(paciente){
            return (
                <Tabs key={uuid.v1()} defaultActiveKey="informacion_general">
                   <TabPane tab={<span><Icon type="user" />Información general</span>} key="informacion_general">
                        <FormHistorial key={uuid.v4()} update={this.update} addContacto={this.addContacto} paciente={paciente} />
                    </TabPane>
                    <TabPane tab={<span><Icon type="folder" />Antecedentes</span>} key="antecedentes">
                        <FormAntecedentes key={uuid.v4()} addHeredoFamiliar={this.addHeredoFamiliar} antecedentes={antecedentes}/>
                    </TabPane>
                    <TabPane tab={<span><Icon type="heart-o" />Salud</span>} key="salud">    
                        <FormSalud  />        
                    </TabPane>
                    <TabPane tab={<span><Icon type="picture" />Odontograma</span>} key="odontograma">

                    </TabPane>
                    <TabPane tab={<span><Icon type="profile" />Plan de Tratamiento</span>} key="plan_tratamiento">
                        <FormPlanTratamiento  />
                    </TabPane>
                    <TabPane tab={<span><Icon type="form" />Consentimiento</span>} key="consentimiento">
                        <FormConsentimiento doctor={this.props.doctor} />
                    </TabPane>
                    <TabPane tab={<span><Icon type="area-chart" />Evolución del Paciente</span>} key="evolucion_paciente">

                    </TabPane>
                    <TabPane tab={<span><Icon type="idcard" />Estado de Cuenta</span>} key="estado_cuenta">
                        <FormEstadoCuenta />
                    </TabPane>

                </Tabs >

            )
        }else{
            return (<Row type="flex" justify="center" align="middle">
                        <Col style={{marginTop: '5em'}}>
                            <Spin size="large"/> Cargando...
                        </Col>
                    </Row>)
        }
    }
    render() {
        return (
            <div>
                <h2>Historial del paciente</h2>
                {this.loader()}
            </div>
        );
    }
}

function mapStateToProps(state, ownProps){
    return {
        paciente: state.historialSelected.paciente,
        antecedentes: state.historialSelected.antecedentes,
        doctor: state.doctor
    }
}
function mapDispatchToProps(dispatch){
    return {
        getPaciente: (id, jwt) => dispatch(historialActions.getPaciente(id, jwt)),
        addContacto: (data) => dispatch(historialActions.addContacto(data)),
        getAntecedentes: (id, jwt) => dispatch(historialActions.getAntecedentes(id, jwt)),
        addHeredoFamiliar: (data) => dispatch(historialActions.addHeredoFamiliar(data))
    }
}
export default connect(mapStateToProps)(withRouter(Historial));