import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Form, Icon, Input, Button, Row, 
        Col, Radio, DatePicker, Select, message } from 'antd';

import * as requestPaciente from '../../requests/requestPaciente';

const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;



class HistorialNuevo extends Component {
    constructor(props){
        super(props);

        this.state = {
            status: false
        }
    }
    handleSelectChange = (value) => {
        // console.log(`selected ${value}`);
        if(value === 'otro'){
            this.setState({status: true});
        } else{
            this.setState({status: false});
        }
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            const { doctor } = this.props;
            const data = {
                nombre: values.nombre,
                ap_paterno: values.ap_paterno,
                ap_materno: values.ap_materno,
                direccion: values.direccion,
                ocupacion: values.ocupacion,
                fecha_nacimiento: values.fecha_nacimiento,
                religion: values.religion,
                escolaridad: values.escolaridad,
                estado_civil: values.estado_civil,
                sexo: values.sexo,
                telefono: `${values.prefix_telefono} ${values.telefono}`
            }
            requestPaciente.create(data, doctor.jwt)
                .then(data => {
                    if(data.status === 200){
                        data = data.data;
                        message.success('Paciente agregado satisfactoriamente!!');
                        this.props.history.push(`/historiales/${data.id}`);
                    }else{
                        message.warning('Algo salio mal :o, recargar la pagina y verificar datos.')
                    }
                    
                }).catch(err => {
                    if(err.response){
                        message.error(err.response.data.msg);
                    }
                })
          }
        });
    }
    render() {
        const { getFieldDecorator } = this.props.form;

        const prefixSelector = getFieldDecorator('prefix_telefono', {
            initialValue: '52',
        })(
            <Select style={{ width: '100px' }}>
                <Option value="52">+52</Option>
                <Option value="1">+1</Option>
                <Option value="">Ninguno</Option>
            </Select>
        );


        return (
            <Row>
                <Col lg={{span: 20, offset: 2}}>
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <FormItem
                            label="Nombre"
                            >
                            {getFieldDecorator('nombre', {
                                rules: [{ required: true, message: 'Ingrese su nombre' }],
                            })(
                                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Nombre" />
                            )}
                        </FormItem>
                        <Row gutter={16}>
                            <Col lg={{span: 12}} xs={{span: 24}}>
                                <FormItem
                                    label="Apellido paterno"
                                    >
                                    {getFieldDecorator('ap_paterno', {
                                        rules: [{ required: true, message: 'Ingrese su apellido paterno' }],
                                    })(
                                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Apellido paterno" />
                                    )}
                                </FormItem>
                            </Col>
                            <Col lg={{span: 12}} xs={{span: 24}}>
                                <FormItem
                                    label="Apellido materno"
                                    >
                                    {getFieldDecorator('ap_materno', {
                                        rules: [{ required: true, message: 'Ingrese su apellido materno' }],
                                    })(
                                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Apellido materno" />
                                    )}
                                </FormItem>
                            </Col>
                            <Col lg={24} xs={24}>
                                <FormItem label="Dirección">
                                    {getFieldDecorator('direccion', {
                                        rules: [{min: 5, message: 'El minimo de caracteres es de 5.'}, {max: 250, message: 'El mensaje debe tener como maximo 500 caracteres.'}]
                                    })(<Input.TextArea style={{maxHeight:'100px' ,width: '100%', height: '30px' }} placeholder="Ingrese su dirección" rows={2}/>)}
                                </FormItem>
                            </Col>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                                <FormItem label="Ocupación">
                                    {getFieldDecorator('ocupacion', {
                                    })(
                                        <Input  placeholder="Ocupación" />
                                    )}
                                </FormItem>
                            </Col>  
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                                <FormItem label="Sexo">
                                    {getFieldDecorator('sexo', {
                                        initialValue: 'h'
                                    })(
                                        <RadioGroup style={{marginLeft: '45px'}}>
                                            <Radio value="h">Hombre</Radio>
                                            <Radio value="m">Mujer</Radio>
                                        </RadioGroup>
                                    )}
                                </FormItem>
                            </Col>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                                <FormItem label="Telefono"  >
                                    {getFieldDecorator('telefono', {
                                        rules: [{ required: true, message: 'El número de telefono es obligatorio' }],
                                    })(
                                        <Input addonBefore={prefixSelector}  style={{ width: '100%' }} />
                                    )}
                                </FormItem>
                            </Col>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                            <FormItem style={{ width: "100%" }} label="Fecha de Nacimiento">
                                {getFieldDecorator('fecha_nacimiento', {
                                    rules: [{ type: 'object', required: true, message: 'La fecha de nacimiento es obligatoria' }],
                                })(
                                    <DatePicker style={{ width: "100%" }} />
                                )}
                            </FormItem>
                            </Col>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                                <FormItem label="Religión"  >
                                    {getFieldDecorator('religion', {
                                        rules: [],
                                    })(
                                        <Input placeholder="Religión" />
                                    )}
                                </FormItem>
                            </Col>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                            <FormItem label="Escolaridad">
                                {getFieldDecorator('escolaridad', {  
                                    rules: [],
                                })(
                                        <Select showSearch placeholder="Escolaridad" optionFilterProp="children"
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} >
                                            <Option value="primaria">Primaria</Option>
                                            <Option value="secundaria">Secundaria</Option>
                                            <Option value="medio-superior">Medio-Superior (Bachillerato)</Option>
                                            <Option value="superior">Superior (Universidad)</Option>    
                                        </Select>  
                                )}
                            </FormItem>
                            </Col>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>     
                                <FormItem label="Estado Civil">
                                    {getFieldDecorator('estado_civil', {
                                        rules: [],
                                    })(
                                        <Select 
                                            mode="combobox"showSearch 
                                            placeholder="Estado Civil" optionFilterProp="children"
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} >
                                            <Option value="soltero">Soltero</Option>
                                            <Option value="viudo">Viudo</Option>
                                            <Option value="casado">Casado</Option>                            
                                        </Select>                              
                                    )}
                                </FormItem>
                                    
                            </Col>   
                        </Row>
                        <Button type="primary" htmlType="submit" className="login-form-button">
                            Guardar
                        </Button>
                    </Form>
                </Col>
            </Row>
        );
    }
}


function mapStateToProps(state, ownState){
    return {
        doctor: state.doctor
    }
}
export default withRouter(connect(mapStateToProps)(Form.create()(HistorialNuevo)));