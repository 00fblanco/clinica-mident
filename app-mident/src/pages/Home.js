import React, { Component } from 'react';
import { Calendar } from 'antd';

class Home extends Component {
    
    onPanelChange(value, mode) {
        // console.log(value, mode);
    }
    render() {
        return (
            <div>
                <h2 style={{color: '#283d52'}}>Calendario de citas</h2>
                <Calendar  onPanelChange={this.onPanelChange()} />
            </div>
        );
    }
}

export default Home;