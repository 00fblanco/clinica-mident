import React, { Component } from 'react';
import { BrowserRouter as Router, Route} from 'react-router-dom';
import { connect } from 'react-redux';

import './App.css';
// containers
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';

class App extends Component {

    signedInRoutes(){
        if(this.props.doctor.jwt){ // Definir rutas cuando inicio sesión
            return (
                    <Route exact path="/*" component={Dashboard}/>
            )
        }
        // rutas cuando no inicio sesión
        return (
            <Route exact path="/*" component={Login}/>
        )
    }
    render() {
        return (
            <Router>
                {this.signedInRoutes()}
            </Router>
        );
    }
}
function mapStateToProps(state, ownProps){
    return {
        doctor: state.doctor
    }
}
export default connect(mapStateToProps)(App);