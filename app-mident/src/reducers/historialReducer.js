export default function historialReducer(state = {}, action){
    switch (action.type) {
        case 'LOAD_PACIENTE':
            return {...state, paciente: action.paciente}
            break;
        case 'LOAD_ANTECEDENTES':
            return {...state, antecedentes: action.antecedentes}
            break;
        case 'ADD_HEREDO_FAMILIAR':
            var stateCopy = Object.assign(state);
            stateCopy.antecedentes.push(action.heredoFamiliar);
            return stateCopy;
            break;   
        case 'ADD_CONTACTO':
            return {
                ...state,
                paciente: {
                    ...state.paciente,
                    contactos_emergencia: state.paciente.contactos_emergencia.concat([action.contacto])
                }
            }
            break;
        default:
            return state;
    }
}