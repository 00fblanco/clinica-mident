export default function pacienteReducer(state = [], action){

    switch (action.type) {
        case 'LOAD_PACIENTES':
            return action.pacientes
            break; 
        
        default:
            return state;
    }
}