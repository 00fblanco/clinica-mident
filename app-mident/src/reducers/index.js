import { combineReducers } from 'redux';

import doctorReducer from './doctorReducer';
import pacienteReducer from './pacienteReducer';
import historialReducer from './historialReducer';
export default combineReducers({
    doctor: doctorReducer,
    pacientes: pacienteReducer,
    historialSelected: historialReducer
})