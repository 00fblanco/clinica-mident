export default function doctorReducer(state = {}, action){
    switch(action.type){
        case 'LOG_IN':
            return {...state, jwt: action.jwt};
        case 'LOAD_DOCTOR':
            return {...state, 
                    nombre: action.doctor.nombre,
                    ap_paterno: action.doctor.ap_paterno,
                    ap_materno: action.doctor.ap_materno, 
                    correo: action.doctor.correo,
                    id: action.doctor.id };
        case 'LOG_OUT':
                return {};
        default:
            return state;
    }
}