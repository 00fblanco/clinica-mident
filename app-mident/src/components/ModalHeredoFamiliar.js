import React, { Component } from 'react';
import { Modal, Form, Input, Select } from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;

class ModalHeredoFamiliar extends Component {

    render() {
        
        const { visible, onCancel, onCreate, form } = this.props;
        const { getFieldDecorator } = form;

        return (

            <Modal
                visible={visible}
                title="Agregar Antecendete Heredo-Familiar"
                okText="Agregar"
                onCancel={onCancel}
                onOk={onCreate}
                >
                <Form layout="vertical">
                    <FormItem label="Parentesco">
                        {getFieldDecorator('parentesco', {
                            rules: [{ required: true, message: 'Por favor introduce el parentesco!' }],
                        })(
                            <Input placeholder="Parentesco" />
                        )}
                    </FormItem>
                    {/* falta sacar las enfermedades de la base de datos */}
                    <FormItem label="Padecimiento">
                        {getFieldDecorator('enfermedades', {
                            rules: [],
                        })(
                            <Select
                                mode="multiple" showSearch
                                placeholder="Seleccione los padecimientos" optionFilterProp="children"
                                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} >
                                <Option value="1">Sida</Option>
                                <Option value="asma">Asma</Option>
                                <Option value="cancer">Cancer</Option>
                            </Select>
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}
export default Form.create()(ModalHeredoFamiliar);
