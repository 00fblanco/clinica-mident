import React, { Component } from 'react';
import { Modal, Form, Input, Select } from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;

class ModalPlanTratamiento extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const data = {
                    tramiento: values.tratamiento,
                    costo: values.costo,
                    observacion: values.observacion,
                }
                data['id'] = this.props.historial.id;
                this.props.update(data);
            }
        });
    }

    render() {
        const { visible, onCancel, onCreate, form } = this.props;
        const { getFieldDecorator } = form;

        return (

            <Modal
                visible={visible}
                title="Agregar Plan de Tratamiento"
                okText="Agregar"
                onCancel={onCancel}
                onOk={onCreate}
                >
                <Form layout="vertical">
                    <FormItem label="Tratamiento">
                        {getFieldDecorator('tratamiento', {
                            rules: [{required:true, message: 'Por favor introduce un tratamiento!'}],
                            /*initialValue: historial.padecimiento || null,*/
                        })(
                            <Select
                                mode="combobox" showSearch
                                placeholder="Tratamiento" optionFilterProp="children"
                                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} >
                                <Option value="limpieza">Limpieza Dental</Option>
                                <Option value="protesis">Protesis Dental</Option>
                                <Option value="ortondocia">Ortodoncia</Option>
                            </Select>
                        )}
                    </FormItem>
                    <FormItem label="Costo">
                        {getFieldDecorator('costo', {
                            rules: [{ required: true, message: 'Por favor introduce el costo!' }],
                        })(
                            <Input placeholder="costo" />
                        )}
                    </FormItem>
                    <FormItem label="Observacion">
                        {getFieldDecorator('observacion', {
                          
                        })(
                            <Input.TextArea placeholder="Observación" />
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}
export default Form.create()(ModalPlanTratamiento);
