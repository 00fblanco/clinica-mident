import React, {Component} from 'react';
import { Form, Input, Button, Row, Col, Radio, Divider } from 'antd';

const FormItem = Form.Item;
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class FormSalud extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const data = {
                    cara: values.cara,
                    cuello: values.padecimiento,
                    atm: values.atm,
                    tejidosBucales: values.tedijosBucales,
                    oclusion: values.oclusion,
                    otros: values.otros
                }
                data['id'] = this.props.historial.id;
                this.props.update(data);
            }
        });
    }

    render(){

        const { getFieldDecorator } = this.props.form;
       
        return(
                <div>
                    <Col lg={{ span: 20, offset: 2 }}>  
                        <Form onSubmit={this.handleSubmit} className="login-form">
                        <Divider>Exploración Física </Divider>
                            <FormItem label="Cara">
                                {getFieldDecorator('cara', {

                                })(
                                    <Input placeholder="Cara"/>
                                )}
                            </FormItem>
                            <FormItem label="Cuello">
                                {getFieldDecorator('cuello', {

                                })(
                                    <Input placeholder="Cuello"/>
                                )}
                            </FormItem>
                            <FormItem label="A.T.M">
                                {getFieldDecorator('atm', {

                                })(
                                    <Input placeholder="A.T.M"/>
                                )}
                            </FormItem>
                            <FormItem label="Cara">
                                {getFieldDecorator('Tejidos Bucales', {

                                })(
                                    <Input placeholder="Tejidos Bucales"/>
                                )}
                            </FormItem>
                            <FormItem label="Oclusión">
                                {getFieldDecorator('oclusion', {

                                })(
                                    <Input placeholder="Oclusión"/>
                                )}
                            </FormItem>
                            <FormItem label="Otros">
                                {getFieldDecorator('otros', {

                                })(
                                    <Input placeholder="Otros"/>
                                )}
                            </FormItem>
                        <Divider> Signos Vitales </Divider>
                            <Row gutter={16}>
                                <Col lg={{ span: 6 }} xs={{ span: 12 }}>
                                    <FormItem label="Peso">
                                        {getFieldDecorator('peso', {

                                        })(
                                            <Input placeholder="peso"/>
                                        )}
                                     </FormItem>
                                 </Col>
                                 <Col lg={{ span: 6 }} xs={{ span: 12 }}>
                                    <FormItem label="Temperatura">
                                        {getFieldDecorator('peso', {

                                        })(
                                            <Input placeholder="peso"/>
                                        )}
                                     </FormItem>
                                 </Col>
                                 <Col lg={{ span: 6 }} xs={{ span: 12 }}>
                                    <FormItem label="Talla">
                                        {getFieldDecorator('peso', {

                                        })(
                                            <Input placeholder="peso" />
                                        )}
                                    </FormItem>
                                </Col>
                                <Col lg={{ span: 6 }} xs={{ span: 12 }}>
                                    <FormItem label="T/A">
                                        {getFieldDecorator('peso', {

                                        })(
                                            <Input placeholder="peso" />
                                        )}
                                    </FormItem>
                                </Col>
                                <Col lg={{ span: 6 }} xs={{ span: 12 }}>
                                    <FormItem label="F.R">
                                        {getFieldDecorator('peso', {

                                        })(
                                            <Input placeholder="peso" />
                                        )}
                                    </FormItem>
                                </Col>
                            </Row>
                            <FormItem label="¿Hábitos para funcionales">
                                {getFieldDecorator('habitosFuncionales', {

                                })(
                                    <Input.TextArea />
                                )}
                            </FormItem>
                        <Divider> Estado de Salud Bucal </Divider>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                                <FormItem label="Higiene">
                                    {getFieldDecorator('gabinete', {

                                    })(
                                        <RadioGroup style={{ marginLeft: 'auto' }}>
                                            <RadioButton value="b">Buena</RadioButton>
                                            <RadioButton value="r">Regular</RadioButton>
                                            <RadioButton value="p">Pobre</RadioButton>
                                            <RadioButton value="d">Deficiente</RadioButton>
                                        </RadioGroup>
                                    )}
                                </FormItem>
                            </Col>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                                <FormItem label="Cepillado">
                                    {getFieldDecorator('cepillado', {

                                    })(
                                        <RadioGroup style={{ marginLeft: 'auto' }}>
                                            <RadioButton value="1">1 vez</RadioButton>
                                            <RadioButton value="2">2 veces</RadioButton>
                                            <RadioButton value="3">3 veces</RadioButton>
                                            <RadioButton value="n">No se cepilla</RadioButton>
                                        </RadioGroup>
                                    )}
                                </FormItem>
                            </Col>
                                 <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                                <FormItem label="Enjuague">
                                    {getFieldDecorator('enjuague', {

                                    })(
                                        <RadioGroup style={{ marginLeft: 'auto' }}>
                                            <RadioButton value="1">1 vez</RadioButton>
                                            <RadioButton value="2">2 veces</RadioButton>
                                            <RadioButton value="3">3 veces</RadioButton>
                                            <RadioButton value="n">No usa</RadioButton>
                                        </RadioGroup>
                                    )}
                                </FormItem>
                                </Col>
                                <Col lg={{ span: 6 }} xs={{ span: 12 }}>
                                    <FormItem label="Hilo Dental">
                                        {getFieldDecorator('hiloDental', {

                                        })(
                                            <RadioGroup style={{ marginLeft: 'auto' }}>
                                                <RadioButton value="s">Si usa</RadioButton>
                                                <RadioButton value="n">No usa</RadioButton>
                                            </RadioGroup>
                                        )}
                                    </FormItem>
                                </Col>
                                <Col lg={{ span: 6 }} xs={{ span: 12 }}>
                                    <FormItem label="Halitosis">
                                        {getFieldDecorator('halitosis', {

                                        })(
                                            <RadioGroup style={{ marginLeft: 'auto' }}>
                                                <RadioButton value="1">Si</RadioButton>
                                                <RadioButton value="2">No</RadioButton>
                                            </RadioGroup>
                                        )}
                                    </FormItem>
                                </Col>
                        <Button icon='sync' type="primary" htmlType="submit" className="login-form-button">
                            Actualizar
                        </Button>
                        </Form>
                     </Col>
                 </div>
        );
    }
}

export default Form.create()(FormSalud);
