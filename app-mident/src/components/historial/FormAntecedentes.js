import React, { Component } from 'react';
import { Form, Input, Button, Row, Col, Radio,
     Divider, Select, Table } from 'antd';
import uuid from 'uuid';

import ModalToxiconomia from '../../components/ModalToxiconomia';
import ModalHeredoFamiliar from '../../components/ModalHeredoFamiliar';

const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

class FormAntecedentes extends Component {

    constructor(props){
        super(props);
        this.state = {
            visibleModalToxiconomia: false,
            visibleModalHeredoFamiliar: false,
        }
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const data = {
                    parentesco: values.familiar,
                    familiarPadecimiento: values.padecimiento,
                    patologico: values.patologico,
                    noPatologico: values.direccion,
                    anestesia: values.anestesia,
                    fecha_nacimiento: values.fecha_nacimiento.format('YYYY-MM-DD'),
                    telefono: `${values.prefix_telefono} ${values.telefono}`
                }
                data['id'] = this.props.historial.id;
            }
        });
    }
    showModalToxiconomia = () => {
        this.setState({ visibleModalToxiconomia:true });
    }

    showModalHeredoFamiliar = () => {
        this.setState({visibleModalHeredoFamiliar:true});
    }

    
    handleCancel = () => {
        this.setState({ 
            visibleModalHeredoFamiliar: false,
            visibleModalToxiconomia: false
         });
    }
    saveFormRefModalHeredoFamiliar = (form) => {
        this.form = form;
    }
            
    handleCreateHeredoFamiliar = () => {
        const form = this.form;
        form.validateFields((err, values) => {
            if (err) {
                return;
            }
            const data = {
                parentesco: values.parentesco,
                enfermedades: values.enfermedades,
            }
            this.props.addHeredoFamiliar(data);
            form.resetFields();
            // porque no tira render despues de agregarlo a redux :c
            this.setState({ visibleModalHeredoFamiliar: false });
        });
    }
    render() {
        const { antecedentes } = this.props;
        const { getFieldDecorator } = this.props.form;
        // dataSource Heredo-Familiares
        const dsHeredoFamiliares = antecedentes ? antecedentes.map(antecedente => {
            return {
                key: uuid.v4(),
                parentesco: antecedente.parentesco_familiar,
                padecimiento: antecedente.id_enfermedad
            }
        }) : [];
        //Checkbox
        const options = [
            { label: 'Diabetes', value: 'diabetes' },
            { label: 'Traumatismo', value: 'traumatismo' },
            { label: 'Asma', value: 'asma' },
            { label: 'Tuberculosis', value: 'tuberculosis' },
            { label: 'Cancer', value: 'cancer' },
            { label: 'Convulsiones', value: 'convulsiones' },
            { label: 'Hipertensión', value: 'hipertension' },
            { label: 'E.T.S', value: 'ets' },
            { label: 'Tranfusiones', value: 'transfuciones' },
            { label: 'Cardiopatía', value: 'cardiopatia' },
            { label: 'Gastritis', value: 'hepatitis' },
            { label: 'Fiebre reumática', value: 'fiebre_reumatica' },
            { label: 'Alergia', value: 'alergia' }
        ];

        //Columns table
        const columns = [{
            title: 'Toxiconomia',
            dataIndex: 'toxiconomia',
            key: 'toxiconomia',
        }, {
            title: 'Cantidad',
            dataIndex: 'cantidad',
            key: 'cantidad',
        }, {
            title: 'F.I',
            dataIndex: 'fi',
            key: 'fi',

        }, {
            title: 'F.F Causa',
            dataIndex: 'ff',
            key: 'ff',
        }];

        const columnsFamiliar = [{
            title: 'Familiar',
            dataIndex: 'parentesco',
            key: 'parentesco',
        }, {
            title: 'Padecimiento(s)',
            dataIndex: 'padecimiento',
            key: 'padecimiento'
        }];

        return(
            <div>
                <Col lg={{ span: 20, offset: 2 }}>
                    <Divider>Heredo-Familiares</Divider>
                    <Form onSubmit={this.handleSubmit} className="login-form">
                        <Row gutter={16}>
                            <Table columns={columnsFamiliar} dataSource={dsHeredoFamiliares} bordered />
                            <div>
                                <Button style={{ float: 'right', marginTop: '10px' }} type="primary" onClick={this.showModalHeredoFamiliar}>Agregar Familiar</Button>
                                <ModalHeredoFamiliar
                                    ref={this.saveFormRefModalHeredoFamiliar}
                                    visible={this.state.visibleModalHeredoFamiliar}
                                    onCancel={this.handleCancel}
                                    onCreate={this.handleCreateHeredoFamiliar}
                                />
                            </div>  
                            <Divider>Patológicos</Divider>
                                <FormItem label="Padecimiento">
                                    {getFieldDecorator('padecimiento', {
                                        rules: [],
                                        /*initialValue: historial.padecimiento || null,*/
                                    })(
                                        <Select
                                            mode="multiple" showSearch
                                            placeholder="Padecimiento" optionFilterProp="children"
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} >
                                            <Option value="diabetes">Diabetes</Option>
                                            <Option value="asma">Asma</Option>
                                            <Option value="cancer">Cancer</Option>
                                        </Select>
                                    )}
                                </FormItem>
                                    <Col lg={{ span: 12 }} xs={{ span: 24 }}> 
                                    <FormItem label="¿Estudios de gabinete?">
                                        {getFieldDecorator('gabinete', {
                                            
                                        })(
                                            <RadioGroup style={{ marginLeft: '80px' }}>
                                                <Radio value="s">Si</Radio>
                                                <Radio value="n">No</Radio>
                                            </RadioGroup>
                                        )}
                                    </FormItem>
                                </Col>
                                    <Col lg={{ span: 12 }} xs={{ span: 24 }}> 
                                    <FormItem label="¿Lo han anestesiado antes?">
                                        {getFieldDecorator('anestesia', {
                                            
                                        })(
                                            <RadioGroup style={{ marginLeft: '80px' }}>
                                                <Radio value="s">Si</Radio>
                                                <Radio value="n">No</Radio>
                                            </RadioGroup>
                                        )}
                                    </FormItem>
                                </Col>
                                    <Col lg={{ span: 12 }} xs={{ span: 24 }}> 
                                    <FormItem label="¿Alguna reacción?">
                                            {getFieldDecorator('alergia', {
                                            
                                        })(
                                            <RadioGroup style={{ marginLeft: '80px' }}>
                                                <Radio value="s">Si</Radio>
                                                <Radio value="n">No</Radio>
                                            </RadioGroup>
                                        )}
                                    </FormItem>
                                </Col>
                                    <Col lg={{ span: 12 }} xs={{ span: 24 }}> 
                                    <FormItem label="¿Alguna intervención quirurgica?">
                                            {getFieldDecorator('intervencionQuirurgica', {
                                            
                                        })(
                                            <RadioGroup style={{ marginLeft: '80px' }}>
                                                <Radio value="s">Si</Radio>
                                                <Radio value="n">No</Radio>
                                            </RadioGroup>
                                        )}
                                    </FormItem>
                                </Col>
                                    <Col lg={{ span: 12 }} xs={{ span: 24 }}> 
                                    <FormItem label="¿Alguna complicación?">
                                            {getFieldDecorator('complicacion', {
                                            
                                        })(
                                            <RadioGroup style={{ marginLeft: '80px' }}>
                                                <Radio value="s">Si</Radio>
                                                <Radio value="n">No</Radio>
                                            </RadioGroup>
                                        )}
                                    </FormItem>
                                </Col>
                                    <FormItem label="¿Toma algún medicamento?">
                                            {getFieldDecorator('medicamento', {
                                            
                                        })(
                                            <RadioGroup style={{ marginLeft: '80px' }}>
                                                <Radio value="s">Si</Radio>
                                                <Radio value="n">No</Radio>
                                            </RadioGroup>
                                        )}
                                    </FormItem>
                            <Divider>Toxicomanías</Divider>
                            <Table columns={columns} /*dataSource={}*/ bordered />
                            <div>
                                <Button style={{float:'right', marginTop:'10px'}} type="primary" onClick={this.showModalToxiconomia}>Agregar Toxiconomia</Button>
                                <ModalToxiconomia
                                    ref={this.saveFormRef}
                                    visible={this.state.visibleModalToxiconomia}
                                    onCancel={this.handleCancel}
                                    onCreate={this.handleCreate}
                                    />
                            </div>          
                            
                            <Divider>Personales (No Patológicos)</Divider>
                                
                                <FormItem label="Higiene Personal">
                                    {getFieldDecorator('higienePersonal', {

                                    })(
                                        <Input placeholder="Higiene Personal"/>
                                    )}
                                </FormItem>
                                <FormItem label="Alimentos inadecuados">
                                    {getFieldDecorator('alimentos', {

                                    })(
                                        <Input placeholder="Alimentos inadecuados" />
                                    )}
                                </FormItem>
                                <FormItem label="Adicciones">
                                    {getFieldDecorator('adicciones', {

                                    })(
                                        <Input placeholder="Adicciones" />
                                    )}
                                </FormItem>
                                <FormItem label="Ejercicio">
                                    {getFieldDecorator('ejercicio', {

                                    })(
                                        <Input placeholder="Ejercicio" />
                                    )}
                                </FormItem>
                                <FormItem label="Inmunizaciones">
                                    {getFieldDecorator('inmunizaciones', {

                                    })(
                                        <Input placeholder="Inmunizaciones" />
                                    )}
                                </FormItem>
                                <FormItem label="Ultima aplicación">
                                    {getFieldDecorator('aplicacion', {

                                    })(
                                        <Input placeholder="Ultima aplicación" />
                                    )}
                                </FormItem>
                                {
                                    this.state.visible ?  

                                        <FormItem label="Ultima aplicación">
                                            {getFieldDecorator('aplicacion', {

                                            })(
                                                <Input placeholder="Ultima aplicación" />
                                            )}
                                        </FormItem>  

                                    :

                                    null
                                }
                            </Row>
                        <Button icon='sync' type="primary" htmlType="submit" className="login-form-button">
                            Actualizar
                        </Button>
                    </Form>
                    </Col>
                 </div>
        );
    }
}

export default Form.create()(FormAntecedentes);
