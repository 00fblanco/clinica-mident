import React, { Component } from 'react';
import { Form, Col,} from 'antd';

class FormConsentimiento extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const data = {
                    cara: values.cara,
                    cuello: values.padecimiento,
                    atm: values.atm,
                    tejidosBucales: values.tedijosBucales,
                    oclusion: values.oclusion,
                    otros: values.otros
                }
                data['id'] = this.props.historial.id;
                this.props.update(data);
            }
        });
    }

    render(){
        const { getFieldDecorator } = this.props.form;
        const { doctor } = this.props;
        return(
            <div>
                <Col lg={{ span: 20, offset: 2 }}>
                         <h3 style={{textAlign:'center'}}>Consentimiento Informado</h3>
                         <p>
                          Yo <strong></strong>
                          declaró y manifiesto en pleno uso de mis facultades mentales, libre y sin ninguna presión la 
                          presion la AUTOTIZACION al<strong> Dr.{`${doctor.nombre} ${doctor.ap_paterno} ${doctor.ap_materno}`}</strong>, CEDULA PROFESIONAL 10298151 que realice el/los
                          tratamientos(s), ue se me han informado de acuerdo a mi grado y tipo de enfermedad incluyendo los estudios
                          radiográficos y analíticos asi como interconsultas con otro servicio médico que se requieran, no quedándome 
                          ninguna duda de mi estado de salud actual y que es imprescindible mi colaboración por tal motivo hago constar que
                          todos los datos que proporciones son veridicos; también autorizo a que se me administre anestésico, analgésicos,
                          antibióticos y cualquier otro tipo de medicamento para mejorar mi estado actual.
                          Estoy entereado(a) de las ventajas y desventajas, así como del costo del tratamiento que elegí y en caso de una 
                          situación inesperada durante la intervención o tratamiento autorizo realizar cualquier otro procecimiento o maniobra
                          distinta a lo establecido que sea necesario y por quien se designe. Se me ha explicado que durante el tratamiento ó después
                          del mismo, se pueden presentar eventos como: inflamación, endurecimiento de tejidos y dolor como parte del proceso de recuperación.
                          Asimismo, comprendo que la odontología no es una ciencia exacta y por ende libero de toda responsabilidad civil, penal o de cualquier
                          índole al personal de salud durante mi proceso.
                          Nombre y firma del paciente
                         </p>
                        <div >
                      
                            {/* // <ReactSignature />  */}
                                
                        </div>
                </Col>
            </div>
        );

    }

}

export default Form.create()(FormConsentimiento);
