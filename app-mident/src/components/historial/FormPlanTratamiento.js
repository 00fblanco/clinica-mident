import React, { Component } from 'react';
import { Form, Button, Col, Divider, Table } from 'antd';

import ModalPlanTratamiento from '../../components/ModalPlanTratamiento';

class FormPlanTratamiento extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const data = {
                    cara: values.cara,
                    cuello: values.padecimiento,
                    atm: values.atm,
                    tejidosBucales: values.tedijosBucales,
                    oclusion: values.oclusion,
                    otros: values.otros
                }
                data['id'] = this.props.historial.id;
                this.props.update(data);
            }
        });
    }

    state = {
        visible: false,
    }


    showModal = () => {
        this.setState({ visible: true });
    }

    handleCancel = () => {
        this.setState({ visible: false });
    }

     // handleCreate = () => {
    //     const form = this.form;
    //     form.validateFields((err, values) => {
    //         if (err) {
    //             return;

    //             console.log('Received values of form: ', values);
    //             form.resetFields();
    //             this.setState({ visible: false });
    //         });
    // }

    // saveFormRef = (form) => {
    //     this.form = form;
    // }

    render(){

        const { getFieldDecorator } = this.props.form;

        //Columns table
        const columns = [{
            title: 'Tratamiento',
            dataIndex: 'tratamiento',
            key: 'costo',
        }, {
            title: 'Costo',
            dataIndex: 'costo',
            key: 'costo',
        }, {
            title: 'Observación',
            dataIndex: 'observacion',
            key: 'observacion',
        }];

        return(
            <div>
                <Col lg={{ span: 20, offset: 2 }}>
                    <Table columns={columns} /*dataSource={}*/ bordered />
                        <div>
                             <Button style={{ float: 'right', marginTop: '10px' }} type="primary" onClick={this.showModal}>Agregar Familiar</Button>
                                <ModalPlanTratamiento
                                    ref={this.saveFormRef}
                                    visible={this.state.visible}
                                    onCancel={this.handleCancel}
                                    onCreate={this.handleCreate}
                                />
                         </div>  
                         <div style={{ marginTop: '50px' }}>
                         <Divider> Información </Divider>
                         <p>
                             Lorem ipsum dolor sit amet, 
                             consectetur adipisicing elit. Distinctio vitae, 
                             recusandae sit ex molestias, natus accusamus sequi enim nobis, 
                             sunt ipsam. Voluptatem amet deserunt, 
                             ea quo quae harum voluptates ducimus. 
                         </p>
                        </div>
                </Col>
            </div>
        );

    }

}

export default Form.create()(FormPlanTratamiento);
