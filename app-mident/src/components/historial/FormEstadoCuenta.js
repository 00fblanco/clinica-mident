import React, { Component } from 'react';
import { Form, Button, Col,  Table } from 'antd';


import ModalEstadoCuenta from '../../components/ModalEstadoCuenta';

class FormEstadoCuenta extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const data = {
 
                }
                data['id'] = this.props.historial.id;
                this.props.update(data);
            }
        });
    }

    state = {
        visible: false,
    }


    showModal = () => {
        this.setState({ visible: true });
    }

    handleCancel = () => {
        this.setState({ visible: false });
    }

    // handleCreate = () => {
    //     const form = this.form;
    //     form.validateFields((err, values) => {
    //         if (err) {
    //             return;

    //             console.log('Received values of form: ', values);
    //             form.resetFields();
    //             this.setState({ visible: false });
    //         });
    // }

    // saveFormRef = (form) => {
    //     this.form = form;
    // }

    render() {

        const { getFieldDecorator } = this.props.form;

        //Columns table
        const columns = [{
            title: 'Tratamiento',
            dataIndex: 'tratamiento',
            key: 'tratamiento',
        }, {
            title: 'Fecha',
            dataIndex: 'fecha',
            key: 'fecha',
        }, {
            title: 'Costo',
            dataIndex: 'costo',
            key: 'costo',
        }, {      
            title: 'A CTA.',
            dataIndex: 'costo',
            key: 'costo',
        }, {
            title: 'Firma',
            dataIndex: 'firma',
            key: 'firma',
        }, {
            title: 'Recibe',
            dataIndex: 'recibe',
            key: 'recibe',
        }];

        return (
            <div>
                <Col lg={{ span: 20, offset: 2 }}>
                    <Table columns={columns} /*dataSource={}*/ bordered />
                    <div>
                        <Button style={{ float: 'right', marginTop: '10px' }} type="primary" onClick={this.showModal}>Agregar Familiar</Button>
                        <ModalEstadoCuenta
                            ref={this.saveFormRef}
                            visible={this.state.visible}
                            onCancel={this.handleCancel}
                            onCreate={this.handleCreate}
                        />
                    </div>
                </Col>
            </div>
        );

    }

}

export default Form.create()(FormEstadoCuenta);
