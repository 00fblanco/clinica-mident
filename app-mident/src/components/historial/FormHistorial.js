import React, { Component } from 'react';
import { Form, Icon, Input, Button, Row, 
    Col, Radio, DatePicker, Divider, Select, Table } from 'antd';
import moment from 'moment';
import uuid from 'uuid';

import ModalContactoEmergencia from '../../components/ModalContactoEmergencia';
const FormItem = Form.Item;
const Option = Select.Option;
const RadioGroup = Radio.Group;

class FormHistorial extends Component {
    
    constructor(props){
        super(props);
        this.state = {
            visibleModalContactoEmergencia: false
        }
    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
          if (!err) {
            const data = {
                nombre: values.nombre,
                ap_paterno: values.ap_paterno,
                ap_materno: values.ap_materno,
                direccion: values.direccion,
                ocupacion: values.ocupacion,
                fecha_nacimiento: values.fecha_nacimiento.format('YYYY-MM-DD'),
                religion: values.religion,
                escolaridad: values.escolaridad,
                estado_civil: values.estado_civil,
                sexo: values.sexo,
                telefono: `${values.prefix_telefono} ${values.telefono}`
            }
            data['id'] = this.props.paciente.id;
            this.props.update(data);
          }
        });
    }


    showModal = () => {
        this.setState({visibleModalContactoEmergencia:true});

    }

    handleCancel = () => {
        this.setState({ visibleModalContactoEmergencia: false });
    }
    handleCreateContactoEmergencia = () => {
        const form = this.form;
        form.validateFields((err, values) => {
          if (err) {
            return;
          }
          const data = {
              nombre: values.nombre,
              direccion: values.direccion,
              parentesco: values.parentesco,
              telefono: values.telefono
          }
          data['id_paciente'] = this.props.paciente.id;
          this.props.addContacto(data);
          form.resetFields();
          // porque no tira render despues de agregarlo a redux :c
          this.setState({ visibleModalContactoEmergencia: false });
        });
    }
    saveFormRef = (form) => {
        this.form = form;
    }
    render() {
        const { paciente } = this.props;
        const { visibleModalContactoEmergencia } = this.state;

        const { getFieldDecorator } = this.props.form;
        const prefixSelector = getFieldDecorator('prefix_telefono', {
            initialValue: '52',
        })(
            <Select style={{ width: '100px' }}>
                <Option value="52">+52</Option>
                <Option value="1">+1</Option>
                <Option value="">Ninguno</Option>
            </Select>
        );
        // console.log('contacts', paciente.contactos_emergencia)
        const contactosEmergencia = paciente.contactos_emergencia.map((contacto, index)=> {
            return {
                key: uuid.v4(),
                nombre: contacto.nombre,
                parentesco: contacto.parentesco,
                telefono: contacto.telefono,
                direccion: contacto.direccion 
            }
        })
        //Columns table
        const columnsContactosEmergencia = [{
            title: 'Nombre',
            dataIndex: 'nombre',
            key: 'nombre',
        }, {
            title: 'Parentesco',
            dataIndex: 'parentesco',
            key: 'parentessco',
        }, {
            title: 'Telefono',
            dataIndex: 'telefono',
            key: 'telefono',
        },
        {
           title: 'Dirección',
           dataIndex: 'direccion',
           key: 'direccion' 
        }];

        return (
            <div key={uuid.v4()}>
                <Col lg={{ span: 20, offset: 2 }}>
                <Form onSubmit={this.handleSubmit} className="login-form">
                        <FormItem
                            label="Nombre"
                            >
                            {getFieldDecorator('nombre', {
                                initialValue: paciente.nombre,
                                rules: [{ required: true, message: 'Ingrese su nombre' }],
                            })(
                                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Nombre" />
                            )}
                        </FormItem>
                        <Row gutter={16}>
                            <Col lg={{span: 12}} xs={{span: 24}}>
                                <FormItem
                                    label="Apellido paterno"
                                    >
                                    {getFieldDecorator('ap_paterno', {
                                        initialValue: paciente.ap_paterno,
                                        rules: [{ required: true, message: 'Ingrese su apellido paterno' }],
                                    })(
                                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Apellido paterno" />
                                    )}
                                </FormItem>
                            </Col>
                            <Col lg={{span: 12}} xs={{span: 24}}>
                                <FormItem
                                    label="Apellido materno"
                                    >
                                    {getFieldDecorator('ap_materno', {
                                        initialValue: paciente.ap_materno,
                                        rules: [{ required: true, message: 'Ingrese su apellido materno' }],
                                    })(
                                        <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Apellido materno" />
                                    )}
                                </FormItem>
                            </Col>
                            <Col lg={24} xs={24}>
                                <FormItem label="Dirección">
                                    {getFieldDecorator('direccion', {
                                        initialValue: paciente.direccion,
                                        rules: [{min: 5, message: 'El minimo de caracteres es de 5.'}, {max: 250, message: 'El mensaje debe tener como maximo 500 caracteres.'}]
                                    })(<Input.TextArea style={{maxHeight:'100px' ,width: '100%', height: '30px' }} placeholder="Ingrese su dirección" rows={2}/>)}
                                </FormItem>
                            </Col>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                                <FormItem label="Ocupación">
                                    {getFieldDecorator('ocupacion', {
                                        initialValue: paciente.ocupacion,
                                    })(
                                        <Input  placeholder="Ocupación" />
                                    )}
                                </FormItem>
                            </Col>  
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                                <FormItem label="Sexo">
                                    {getFieldDecorator('sexo', {
                                        initialValue: paciente.sexo
                                    })(
                                        <RadioGroup style={{marginLeft: '45px'}}>
                                            <Radio value="h">Hombre</Radio>
                                            <Radio value="m">Mujer</Radio>
                                        </RadioGroup>
                                    )}
                                </FormItem>
                            </Col>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                                <FormItem label="Telefono"  >
                                    {getFieldDecorator('telefono', {
                                        initialValue: paciente.telefono,
                                        rules: [{ required: true, message: 'El número de telefono es obligatorio' }],
                                    })(
                                        <Input addonBefore={prefixSelector}  style={{ width: '100%' }} />
                                    )}
                                </FormItem>
                            </Col>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                            <FormItem style={{ width: "100%" }} label="Fecha de Nacimiento">
                                {getFieldDecorator('fecha_nacimiento', {
                                    initialValue: moment(paciente.fecha_nacimiento, 'YYYY/MM/DD'),
                                    rules: [{ type: 'object', required: true, message: 'La fecha de nacimiento es obligatoria' }],
                                })(
                                    <DatePicker dateFormat='YYYY-MM-DD' style={{ width: "100%" }} />
                                )}
                            </FormItem>
                            </Col>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                                <FormItem label="Religión"  >
                                    {getFieldDecorator('religion', {
                                        rules: [],
                                        initialValue: paciente.religion,
                                    })(
                                        <Input placeholder="Religión" />
                                    )}
                                </FormItem>
                            </Col>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>
                            <FormItem label="Escolaridad">
                                {getFieldDecorator('escolaridad', {  
                                    rules: [],
                                    initialValue: paciente.escolaridad,
                                })(
                                        <Select showSearch placeholder="Escolaridad" optionFilterProp="children"
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} >
                                            <Option value="primaria">Primaria</Option>
                                            <Option value="secundaria">Secundaria</Option>
                                            <Option value="medio-superior">Medio-Superior (Bachillerato)</Option>
                                            <Option value="superior">Superior (Universidad)</Option>    
                                        </Select>  
                                )}
                            </FormItem>
                            </Col>
                            <Col lg={{ span: 12 }} xs={{ span: 24 }}>     
                                <FormItem label="Estado Civil">
                                    {getFieldDecorator('estado_civil', {
                                        rules: [],
                                        initialValue: paciente.estado_civil || '',
                                    })(
                                        <Select 
                                            mode="combobox" showSearch 
                                            placeholder="Estado Civil" optionFilterProp="children"
                                            filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} >
                                            <Option value="soltero">Soltero</Option>
                                            <Option value="viudo">Viudo</Option>
                                            <Option value="casado">Casado</Option>                            
                                        </Select>                              
                                    )}
                                </FormItem>
                                    
                            </Col>   
                            <Divider>Contacto de Emergencia</Divider>
                            <Table key={uuid.v4()} columns={columnsContactosEmergencia} dataSource={contactosEmergencia} bordered  pagination={{ pageSize: 5 }} scroll={{ x: 1000 }}/>
                            <div>
                                <Button style={{ float: 'right', marginTop: '10px' }} type="primary" onClick={this.showModal}>Agregar Contacto</Button>
                                <ModalContactoEmergencia
                                    ref={this.saveFormRef}
                                    visible={visibleModalContactoEmergencia}
                                    onCancel={this.handleCancel}
                                    onCreate={this.handleCreateContactoEmergencia}
                                />
                            </div>  
                        </Row>
                        <Button icon='sync' type="primary" htmlType="submit" className="login-form-button">
                            Actualizar
                        </Button>
                    </Form>
                </Col>
            </div>
        );
    }
}

export default Form.create()(FormHistorial);
