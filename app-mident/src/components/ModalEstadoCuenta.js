import React, { Component } from 'react';
import { Modal, Form, Input, Select } from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;

class ModalEstadoCuenta extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const data = {
                    tratamiento: values.tratamiento,
                    fecha: values.fecha,
                    costo: values.costo,
                    acta: values.acta,
                    firma: values.firma,
                    recibe: values.recibe
                }
                data['id'] = this.props.historial.id;
                this.props.update(data);
            }
        });
    }

    render() {
        const { visible, onCancel, onCreate, form } = this.props;
        const { getFieldDecorator } = form;

        return (

            <Modal
                visible={visible}
                title="Agregar Plan de Tratamiento"
                okText="Agregar"
                onCancel={onCancel}
                onOk={onCreate}
            >
                <Form layout="vertical">
                    <FormItem label="Tratamiento">
                        {getFieldDecorator('tratamiento', {
                            rules: [{ required: true, message: 'Por favor introduce un tratamiento!' }],
                            /*initialValue: historial.padecimiento || null,*/
                        })(
                            <Select
                                mode="combobox" showSearch
                                placeholder="Tratamiento" optionFilterProp="children"
                                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0} >
                                <Option value="limpieza">Limpieza Dental</Option>
                                <Option value="protesis">Protesis Dental</Option>
                                <Option value="ortondocia">Ortodoncia</Option>
                            </Select>
                        )}
                    </FormItem>
                    <FormItem label="Fecha">
                        {getFieldDecorator('fecha', {
                            rules: [{ required: true, message: 'Por favor introduce la fecha!' }],
                        })(
                            <Input placeholder="fecha" />
                        )}
                    </FormItem>
                    <FormItem label="Costo">
                        {getFieldDecorator('costo', {
                            rules: [{ required: true, message: 'Por favor introduce el costo!' }],
                        })(
                            <Input placeholder="costo" />
                        )}
                    </FormItem>
                    <FormItem label="A CTA.">
                        {getFieldDecorator('acta', {
                            rules: [{ required: true, message: 'Por favor introduce el A CTA.!' }],
                        })(
                            <Input placeholder="A CTA." />
                        )}
                    </FormItem>
                    <FormItem label="Firma">
                        {getFieldDecorator('firma', {

                        })(
                            <Input.TextArea placeholder="Observación" />
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}
export default Form.create()(ModalEstadoCuenta);
