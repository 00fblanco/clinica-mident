import React from 'react';
import { Row, Col } from 'antd';
const notFound = () => {
    return (
        <Row>
            <Col span={12} offset={6} style={{textAlign: 'center'}} >
                <img src="/images/404.gif" alt="" style={{maxWidth: '100%'}}/>
                <p style={{color: '#283d52', fontSize: '2rem', textAlign: 'center'}}>Página no encontrada</p>
            </Col>
        </Row>
        
    )
}

export default notFound;