import React, { Component } from 'react';
import { Modal, Form, Input } from 'antd';

const FormItem = Form.Item;

class ModalToxiconomia extends Component {

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                const data = {
                    toxiconomia: values.toxiconomia,
                    cantidad: values.cantidad,
                    fi: values.fi,
                    ff: values.ff
                }
                data['id'] = this.props.historial.id;
                this.props.update(data);
            }
        });
    }

    render() {

            const { visible, onCancel, onCreate, form } = this.props;
            const { getFieldDecorator } = form;

        return(

                <Modal
                visible={visible}
                title="Agregar Toxiconomia"
                okText="Agregar"
                onCancel={onCancel}
                onOk={onCreate}
                >
                <Form layout="vertical">
                    <FormItem label="Toxiconomia">
                        {getFieldDecorator('toxiconomia', {
                            rules: [{ required: true, message: 'Por favor introduce una toxiconomia!' }],
                        })(
                            <Input placeholder="Toxiconomia"/>
                        )}
                    </FormItem>
                    <FormItem label="Cantidad">
                        {getFieldDecorator('cantidad', {
                            
                        })(
                            <Input />
                        )}
                    </FormItem>
                    <FormItem  label="F.I">
                        {getFieldDecorator('fi', {
                         
                        })(
                           <Input.TextArea />
                        )}
                    </FormItem>
                    <FormItem label="F.F Causa">
                        {getFieldDecorator('ff', {

                        })(
                            <Input.TextArea />
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}
export default Form.create()(ModalToxiconomia);
