import React, { Component } from 'react';
import {  Modal, Form, Input, Select } from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;

class ModalContactoEmergencia extends Component {
    render() {

        const { visible, onCancel, onCreate, form } = this.props;
        const { getFieldDecorator } = form;

        return (

            <Modal
                visible={visible}
                title="Agregar TContacto de Emergencia"
                okText="Agregar"
                onCancel={onCancel}
                onOk={onCreate}
            >
                <Form layout="vertical">
                    <FormItem label="Nombre">
                        {getFieldDecorator('nombre', {
                            rules: [{ required: true, message: 'Por favor introduce un nombre!' }],
                        })(
                            <Input placeholder="Nombre" />
                        )}
                    </FormItem>
                    <FormItem label="Dirección">
                        {getFieldDecorator('direccion', {

                        })(
                            <Input.TextArea style={{ maxHeight: '100px', width: '100%', height: '30px' }} placeholder="Direccion"/>
                        )}
                    </FormItem>
                    <FormItem label="Parentesco">
                        {getFieldDecorator('parentesco', {
                            rules: [{ required: true, message: 'El parentesco es obligatorio'}]
                        })(
                            <Select 
                                placeholder="Seleccione el parentesco"
                                >
                                <Option value="padre">Padre</Option>
                                <Option value="madre">Madre</Option>
                                <Option value="abuelo">Abuelo</Option>
                                <Option value="abuela">Abuela</Option>
                            </Select>
                        )}
                    </FormItem>
                    <FormItem label="Teléfono">
                        {getFieldDecorator('telefono', {

                        })(
                            <Input placeholder="Teléfono" />
                        )}
                    </FormItem>
                </Form>
            </Modal>
        );
    }
}
export default Form.create()(ModalContactoEmergencia);
