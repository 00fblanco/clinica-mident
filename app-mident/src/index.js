import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

// config LocaleProvider
import { LocaleProvider } from 'antd';
import esES from '../node_modules/antd/lib/locale-provider/es_ES';
import * as moment from 'moment/moment';
import '../node_modules/moment/locale/es';

import App from './App';
import { store } from './store/index';
import registerServiceWorker from './registerServiceWorker';


moment.locale('es');
const rootComponent = (
    <Provider store={store}>
        <LocaleProvider locale={esES}>
            <App/>
        </LocaleProvider>
    </Provider>
);
ReactDOM.render(rootComponent,
    document.getElementById('root'))
    
registerServiceWorker();

