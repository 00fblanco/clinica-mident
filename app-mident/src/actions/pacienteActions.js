import * as requestPaciente from '../requests/requestPaciente';

// LOAD paciente to store
export function loadPacientes(pacientes){
    return { type: 'LOAD_PACIENTES', pacientes};
}
export function getPacientes(jwt){
    return (dispatch, getState) => {
        requestPaciente.getPacientes(jwt)
            .then(pacientes => {
                dispatch(loadPacientes(pacientes.data));
            })
    }
}