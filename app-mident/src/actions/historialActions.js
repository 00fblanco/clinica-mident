import * as requestHistorial from '../requests/requestHistorial';

// LOAD historial to store
export function loadPaciente(paciente){
    return { type: 'LOAD_PACIENTE', paciente}
}

export function getPaciente(id, jwt){
    return (dispatch, getState) => {
        requestHistorial.getPaciente(id, jwt)
            .then(paciente => {
                dispatch(loadPaciente(paciente.data));
            })
    }
}

// LOAD antecedentes to store
export function loadAntecedentes(antecedentes){
    return { type: 'LOAD_ANTECEDENTES', antecedentes}
}
export function getAntecedentes(id, jwt){
    return (dispatch, getState) => {
        requestHistorial.getAntecedentes(id, jwt)
            .then(antecedentes => {
                dispatch(loadAntecedentes(antecedentes.data));
            });
    }
}

// ADD heredo familiar
export function addHeredoFamiliar(heredoFamiliar){
    return {type: 'ADD_HEREDO_FAMILIAR', heredoFamiliar}
}
// ADD contacto
export function addContacto(contacto){
    return { type: 'ADD_CONTACTO', contacto}
}
