export function login(jwt){
    return { type: 'LOG_IN', jwt};
}

export function loadDoctor(doctor){
    return {type: 'LOAD_DOCTOR', doctor};
}

export function logout(){
    return { type: 'LOG_OUT'};
}