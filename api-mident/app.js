const express = require('express');
const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const jwtMiddleware = require('express-jwt');

//secrets
const secrets = require('./config/secrets');

// midlewares
const allowCors = require('./middlewares/allowCors')();

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(methodOverride());
app.use(methodOverride("_method"));

app.use(allowCors.unless({path: '/public'}));
app.use(
	jwtMiddleware({secret: secrets.jwtSecret})
		.unless({ path: ['/doctores/auth', '/doctores'], method: ['OPTIONS']})
);

// routes
const doctores = require('./routes/doctores');
const pacientes = require('./routes/pacientes');

app.use('/doctores', doctores);
app.use('/api/pacientes', pacientes);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
	const err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handler
app.use(function(err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.json(err);
});


module.exports = app;