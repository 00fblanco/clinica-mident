const Paciente = require('../db/models').Paciente;
const antecedente_heredo_familiares_enfermedades = require('../db/models').antecedente_heredo_familiares_enfermedades;
const Enfermedad = require('../db/models').Enfermedad;
const ContactoEmergencia = require('../db/models').ContactoEmergencia;
const paramsBuilder = require('./helpers').paramsBuilder;
const Sequelize = require('../db/models').Sequelize;


const validParams = ['nombre', 'ap_paterno', 'ap_materno', 'direccion',
                    'ocupacion', 'fecha_nacimiento', 'religion', 'escolaridad',
                    'estado_civil', 'sexo', 'telefono'];

function create(req, res){
    const params = paramsBuilder(validParams, req.body);
    // console.log('user', req.user)
    params['id_doctor'] = req.user.id;
    Paciente.create(params)
        .then(paciente => {
            res.status(200).json(paciente);
        }).catch(Sequelize.ValidationError, (err) => {
            var errores = err.errors.map((element) => {
                return `${element.path}: ${element.message}`
            })
            // console.log('Sequelize error: ', err)
            res.status(202).json({errores})
        }).catch((err) => {
            console.log(err)
            res.status(406).json({err: err})
        })
}
function update(req, res){
    const params = paramsBuilder(validParams, req.body);
    const id = req.params.id;
    Paciente.update(params, {where: {id}})
        .then(paciente => {
            res.status(200).json(paciente);
        }).catch(Sequelize.ValidationError, (err) => {
            var errores = err.errors.map((element) => {
                return `${element.path}: ${element.message}`
            })
            console.log('Sequelize error: ', err)
            res.status(202).json({errores})
        }).catch((err) => {
            console.log(err)
            res.status(406).json({err: err})
        })
}

// Agregar contacto de emergencia al paciente
function addContacto(req, res){
    let validParams = ['nombre', 'parentesco', 'direccion', 'telefono', 'id_paciente'];
    const params = paramsBuilder(validParams, req.body);
    ContactoEmergencia.create(params)
        .then(contacto => {
            res.status(200).json(contacto);
        }).catch(Sequelize.ValidationError, (err) => {
            var errores = err.errors.map((element) => {
                return `${element.path}: ${element.message}`
            })
            // console.log('Sequelize error: ', err)
            res.status(202).json({errores})
        }).catch((err) => {
            console.log(err)
            res.status(406).json({err: err})
        })
}

// add heredo-familiar to paciente
function addHeredoFamiliar(req, res){
    let validParams = ['id_paciente', 'parentesco_familiar', 'id_enfermedad'];
    const params = paramsBuilder(validParams, req.body);
    antecedente_heredo_familiares_enfermedades.create(params)
        .then(heredo_familiar => {
            res.status(200).json(heredo_familiar)
        }).catch(Sequelize.ValidationError, (err) => {
            var errores = err.errors.map((element) => {
                return `${element.path}: ${element.message}`
            })
            // console.log('Sequelize error: ', err)
            res.status(202).json({errores})
        }).catch((err) => {
            console.log(err)
        })
           
}

function getHistorial(req, res){
    const id_doctor = req.user.id; // para que solo obtenga a los usuario de un doctor
    Paciente.findOne(
            {
                include: [{model: ContactoEmergencia, as: 'contactos_emergencia'}],
                where: {id: req.params.id, id_doctor}
            })
        .then(paciente => {
            res.status(200).json(paciente);
        }).catch((err) => {
            console.log(err)
            res.status(406).json({err: err})
        })
}

function getAntecedentes(req, res){
    const id_paciente = req.params.id;
    antecedente_heredo_familiares_enfermedades.findAll({
        include: [{ model: Enfermedad, as: 'enfermedades'}],
        where: {id_paciente: id_paciente}
    }).then(antecedentes => {
        res.status(200).json(antecedentes);
    }).catch((err) => {
        console.log('ERROR', err)
        res.status(406).json({err: err})
    })
}

function getAll(req, res){
    const id_doctor = req.user.id; // para que solo obtenga a los usuarios de un doctor
    Paciente.findAll({where: {id_doctor}, attributes: ['id', 'nombre', 'ap_paterno', 'ap_materno']})
        .then(pacientes => {
            res.status(200).json(pacientes);
        }).catch((err) => {
            console.log(err)
            res.status(406).json({err: err})
        })
}




module.exports = {
    create,
    update,
    getHistorial,
    getAntecedentes,
    getAll,
    addContacto,
    addHeredoFamiliar
}