const Doctor = require('../db/models').Doctor;
const jwt = require('jsonwebtoken');
const secrets = require('../config/secrets');
const paramsBuilder = require('./helpers').paramsBuilder;
var bCrypt = require('bcrypt-nodejs');


const validParams = ['nombre', 'ap_paterno', 'ap_materno', 'correo', 'contrasenia'];

function create(req, res, next){
    let params = paramsBuilder(validParams, req.body);
    Doctor.create(params)
        .then(doctor => {
            req.doctor = doctor;
            next();
        }).catch(error => {
            console.log(error);
            next(error);
        })
}

function authenticate(req, res, next){
    const correo = req.body.correo;
    const contrasenia = req.body.contrasenia;
    var isValidContrasenia = (doctor_contrasenia, contrasenia) => {
        return bCrypt.compareSync(contrasenia, doctor_contrasenia);
    }
    Doctor.findOne({ where: {correo: correo} })
        .then(doctor => {  
            if(!doctor){
                return res.status(401).json({success: false, msg: 'Autenticación fallo, correo no encontrado.'});
            }
            
            if(!isValidContrasenia(doctor.contrasenia, contrasenia)){
                return res.status(401).json({success: false, msg: 'Autenticación fallo, contraseña incorrecta.'})
            }
            req.doctor = doctor
            next();
        }).catch(err => {
            console.log(err);
        })
}

function generateToken(req, res, next){
    if(!req.doctor) return next();
    req.token = jwt.sign({id: req.doctor.id, correo: req.doctor.correo }, secrets.jwtSecret, {expiresIn: '24h'});
    next();
}

function sendToken(req, res){
    if(req.doctor){
        res.json({
            doctor: req.doctor,
            jwt: req.token
        });
    }else{
        res.status(422).json({ error: 'Autenticación fallo, al generar el token.'})
    }
}

module.exports = {
    create,
    authenticate,
    generateToken,
    sendToken
}