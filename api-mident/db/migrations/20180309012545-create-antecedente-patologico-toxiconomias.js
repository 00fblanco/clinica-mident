'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('antecedentes_patologicos_toxiconomias', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      fi: {
        type: Sequelize.STRING
      },
      ff_causa: {
        type: Sequelize.STRING
      },
      cantidad: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      id_paciente: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'pacientes',
          key: 'id',
          as: 'id_paciente'
        }
      },
      id_toxiconomia: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'toxiconomias',
          key: 'id',
          as: 'id_toxiconomia'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('antecedentes_patologicos_toxiconomias');
  }
};