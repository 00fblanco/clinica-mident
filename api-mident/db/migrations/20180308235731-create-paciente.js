'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('pacientes', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      nombre: {
        type: Sequelize.STRING(60),
        allowNull: false,
      },
      ap_paterno: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      ap_materno: {
        type: Sequelize.STRING(50),
        allowNull: false,
      },
      direccion: {
        type: Sequelize.STRING(250),
      },
      ocupacion: {
        type: Sequelize.STRING(45)
      },
      fecha_nacimiento: {
        type: Sequelize.DATEONLY,
        allowNull: false,
      },
      religion: {
        type: Sequelize.STRING(45)
      },
      escolaridad: {
        type: Sequelize.STRING(45)
      },
      estado_civil: {
        type: Sequelize.STRING(45)
      },
      sexo: {
        type: Sequelize.ENUM,
        values: ['h', 'm']
      },
      telefono: {
        type: Sequelize.STRING(20),
      },
      id_doctor: {
        type: Sequelize.INTEGER,
        allowNull: false,
        onDelete: 'CASCADE',
        references: {
          model: 'doctores',
          key: 'id',
          as: 'id_usuarios'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('pacientes');
  }
};