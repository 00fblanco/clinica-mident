'use strict';
module.exports = (sequelize, DataTypes) => {
  var antecedente_patologico_preguntas = sequelize.define('antecedente_patologico_preguntas', {
    respuesta: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'La respuesta no puede estar vacia'}
      },
    }
  }, {tableName: 'antecedente_patologico_preguntas'});
  antecedente_patologico_preguntas.associate = function(models) {
    antecedente_patologico_preguntas.belongsTo(models.Pregunta, {
      foreignKey: 'id_pregunta',
      onDelete: 'CASCADE',
      as: 'pregunta'
    });
    antecedente_patologico_preguntas.belongsTo(models.Paciente, {
      foreignKey: 'id_paciente',
      onDelete: 'CASCADE',
      as: 'paciente'
    })
  };
  return antecedente_patologico_preguntas;
};