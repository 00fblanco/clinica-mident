'use strict';
module.exports = (sequelize, DataTypes) => {
  var antecedente_patologico_toxiconomias = sequelize.define('antecedente_patologico_toxiconomias', {
    fi: {
      type: DataTypes.STRING
    },
    ff_causa: {
      type: DataTypes.STRING
    },
    cantidad: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {tableName: 'antecedentes_patologicos_toxiconomias'});
  antecedente_patologico_toxiconomias.associate = function(models) {
    antecedente_patologico_toxiconomias.belongsTo(models.Paciente,{
      foreignKey: 'id_paciente',
      onDelete: 'CASCADE',
      as: 'paciente'
    });
    antecedente_patologico_toxiconomias.belongsTo(models.Toxiconomia,{
      foreignKey: 'id_toxiconomia',
      onDelete: 'CASCADE',
      as: 'toxiconomia'
    });
  };
  return antecedente_patologico_toxiconomias;
};