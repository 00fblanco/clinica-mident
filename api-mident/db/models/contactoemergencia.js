'use strict';
module.exports = (sequelize, DataTypes) => {
  var ContactoEmergencia = sequelize.define('ContactoEmergencia', {
    nombre: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'El nombre es obligatorio'}
      }
    },
    parentesco: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'El parentesco es obligatorio'}
      }
    },
    direccion: {
      type: DataTypes.STRING,
    },
    telefono: {
      type: DataTypes.STRING,
    }
  }, {tableName: 'contactos_emergencia'});

  ContactoEmergencia.associate = function(models) {
    ContactoEmergencia.belongsTo(models.Paciente, {
      foreignKey: 'id_paciente',
      onDelete: 'CASCADE',
      as: 'paciente'
    })
  };
  return ContactoEmergencia;
};