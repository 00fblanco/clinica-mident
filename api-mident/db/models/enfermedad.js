'use strict';
module.exports = (sequelize, DataTypes) => {
  var Enfermedad = sequelize.define('Enfermedad', {
    nombre: {
      type: DataTypes.STRING,
      allowNull: false,
      validate:{
        notEmpty: { msg: 'El nombre es obligatorio'},
        min: 3,
        max: 100
      }
    },
    descripcion: {
      type: DataTypes.STRING,
    }
  }, {tableName: 'enfermedades'});
  Enfermedad.associate = function(models) {
    Enfermedad.hasMany(models.antecedente_patologico_enfermedades, {
      foreignKey: 'id_enfermedad',
      as: 'antecedente_patologico_enfermedades'
    });

    Enfermedad.hasMany(models.antecedente_heredo_familiares_enfermedades, {
      foreignKey: 'id_enfermedad',
      as: 'antecedente_heredo_familiares_enfermedades'
    });
  };
  return Enfermedad;
};