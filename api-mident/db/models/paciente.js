'use strict';
module.exports = (sequelize, DataTypes) => {
  var Paciente = sequelize.define('Paciente', {
    nombre: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'El nombre es obligatorio'}
      }
    },
    ap_paterno: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'El apellido paterno es obligatorio'}
      }
    },
    ap_materno: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'El apellido materno es obligatorio'}
      }
    },
    direccion: {
      type: DataTypes.STRING,
    },
    ocupacion: {
      type: DataTypes.STRING
    },
    fecha_nacimiento: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'La fecha de nacimiento es obligatoria'},
        isDate: { msg: 'La fecha debe tener formato DATEONLY'} 
      }
    },
    religion: {
      type: DataTypes.STRING
    },
    escolaridad: {
      type: DataTypes.STRING
    },
    estado_civil: {
      type: DataTypes.STRING
    },
    sexo: {
      type: DataTypes.ENUM,
      values: ['h', 'm']
    },
    telefono: {
      type: DataTypes.STRING,
      validate: {
        min: 5,
        notEmpty: {msg: 'El telefono es obligatorio'}
      }
    },
  }, {tableName: 'pacientes'});
  Paciente.associate = models => {
    Paciente.belongsTo(models.Doctor, {
      foreignKey: 'id_doctor',
      onDelete: 'CASCADE',
      as: 'doctor'
    })
    Paciente.hasMany(models.ContactoEmergencia, {
      foreignKey: 'id_paciente',
      as: 'contactos_emergencia'
    })
    Paciente.hasMany(models.antecedente_patologico_preguntas, {
      foreignKey: 'id_paciente',
      as: 'antecede_patologico_preguntas'
    });

    Paciente.hasMany(models.antecedente_patologico_toxiconomias,{
      foreignKey: 'id_paciente',
      as: 'antecedente_patologico_toxiconomias'
    })

    Paciente.hasMany(models.antecedente_patologico_enfermedades, {
      foreignKey: 'id_paciente',
      as: 'antecedente_patologico_enfermedades'
    });

    Paciente.hasMany(models.antecedente_heredo_familiares_enfermedades, {
      foreignKey: 'id_paciente',
      as: 'antecedente_heredo_familiares_enfermedades'
    });
  }
  return Paciente;
};