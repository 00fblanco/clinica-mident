'use strict';
const bCrypt = require('bcrypt-nodejs');
const generateHash = (contrasenia) => {
  return bCrypt.hashSync(contrasenia, bCrypt.genSaltSync(8), null);
}

module.exports = (sequelize, DataTypes) => {
  var Doctor = sequelize.define('Doctor', {
    nombre: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'El nombre es obligatorio'}
      }
    },
    ap_paterno: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'El nombre es obligatorio'}
      }
    },
    ap_materno: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'El nombre es obligatorio'}
      }
    },
    correo: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {msg: 'El correo es obligatorio'},
        notEmpty: {msg: 'El correo es obligatorio'}
      }
    },
    contrasenia:{
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'La contraseña es obligatoria'}
      }
    },
  }, {
    tableName: 'doctores'
  });
  Doctor.beforeCreate((doctor, options) => {
    doctor.contrasenia = generateHash(doctor.contrasenia);
  });

  Doctor.associate = models =>{
    Doctor.hasMany(models.Paciente, {
      foreignKey: 'id_doctor',
      as: 'pacientes'
    });
  }

  return Doctor;
};