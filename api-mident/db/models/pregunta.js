'use strict';
module.exports = (sequelize, DataTypes) => {
  var Pregunta = sequelize.define('Pregunta', {
    pregunta: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {msg: 'La contraseña es obligatoria'},
        min: 5,
        max: 150
      }
    },
    tipo: {
      type: DataTypes.ENUM,
      values: ['patologicas', 'no_patologicas', 'exploracion_fisica', 'signos_vitales', 'salud_bucal', 'odontograma'],
      allowNull: false
    }
  }, {tableName: 'preguntas'});
  Pregunta.associate = function(models) {
    Pregunta.hasMany(models.antecedente_patologico_preguntas, {
      foreignKey: 'id_pregunta',
      as: 'antecede_patologico_preguntas'
    })
  };
  return Pregunta;
};