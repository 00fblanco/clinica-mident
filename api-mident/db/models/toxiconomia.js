'use strict';
module.exports = (sequelize, DataTypes) => {
  var Toxiconomia = sequelize.define('Toxiconomia', {
    nombre: {
      type: DataTypes.STRING,
      allowNull: false,
      validate:{
        notEmpty: { msg: 'El nombre es obligatorio'},
        min: 3,
        max: 100
      }
    },
    descripcion: {
      type: DataTypes.STRING,
    }
  }, {tableName: 'toxiconomias'});
  
  Toxiconomia.associate = function(models) {
    Toxiconomia.hasMany(models.antecedente_patologico_toxiconomias,{
      foreignKey: 'id_toxiconomia',
      as: 'antecedente_patologico_toxiconomias'
    })
  };
  return Toxiconomia;
};