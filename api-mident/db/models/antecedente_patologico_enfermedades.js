'use strict';
module.exports = (sequelize, DataTypes) => {
  var antecedente_patologico_enfermedades = sequelize.define('antecedente_patologico_enfermedades', {

  }, {tableName: 'antecedentes_patologico_enfermedades'});
  antecedente_patologico_enfermedades.associate = function(models) {
    antecedente_patologico_enfermedades.belongsTo(models.Enfermedad, {
      foreignKey: 'id_enfermedad',
      onDelete: 'CASCADE',
      as: 'enfermedad'
    });

    antecedente_patologico_enfermedades.belongsTo(models.Paciente, {
      foreignKey: 'id_paciente',
      onDelete: 'CASCADE',
      as: 'paciente'
    });
  };
  return antecedente_patologico_enfermedades;
};