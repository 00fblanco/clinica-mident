'use strict';
module.exports = (sequelize, DataTypes) => {
  var antecedente_heredo_familiares_enfermedades = sequelize.define('antecedente_heredo_familiares_enfermedades', {
    parentesco_familiar: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: { msg: 'Debe indicar el parentesco'}
      }
    }
  }, {tableName: 'antecedentes_heredo_familiares_enfermedades'});

  antecedente_heredo_familiares_enfermedades.associate = function(models) {
    antecedente_heredo_familiares_enfermedades.belongsTo(models.Enfermedad, {
      foreignKey: 'id_enfermedad',
      onDelete: 'CASCADE',
      as: 'enfermedades'
    });

    antecedente_heredo_familiares_enfermedades.belongsTo(models.Paciente, {
      foreignKey: 'id_paciente',
      onDelete: 'CASCADE',
      as: 'paciente'
    });
  };
  return antecedente_heredo_familiares_enfermedades;
};