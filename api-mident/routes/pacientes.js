const express = require('express');
const router = express.Router();
const jwtMiddleware = require('express-jwt');

const pacientesController = require('../controllers/PacientesController');

router.route('/')
    .get(pacientesController.getAll)
    .post(pacientesController.create)

router.route('/contactos_emergencia')
    .post(pacientesController.addContacto)
    
router.route('/antecedentes/')
    .post(pacientesController.addHeredoFamiliar)
    
router.route('/:id/antecedentes')
    .get(pacientesController.getAntecedentes)

router.route('/:id')
    .get(pacientesController.getHistorial)
    .put(pacientesController.update)



module.exports = router;