const express = require('express');
const router = express.Router();
const jwtMiddleware = require('express-jwt');


const doctoresController = require('../controllers/DoctoresController');

router.route('/')
    .post(doctoresController.create,
        doctoresController.generateToken,
        doctoresController.sendToken)

router.route('/auth')
    .post(doctoresController.authenticate,
        doctoresController.generateToken,
        doctoresController.sendToken);

module.exports = router;